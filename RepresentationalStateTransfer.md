 # Representational state transfer (REST) architecture.

# Abstract : 

**Representational state transfer** (REST) is a software architectural style that was created to guide the design and development of the architecture for the [World Wide Web](https://en.wikipedia.org/wiki/World_Wide_Web). REST defines a set of constraints for how the architecture of an Internet-scale distributed [hypermedia](https://en.wikipedia.org/wiki/Hypermedia) system, such as the Web.

REST has been employed throughout the software industry and is a widely accepted set of guidelines for creating stateless, reliable **web services**.

![REST](https://miro.medium.com/proxy/1*EbBD6IXvf3o-YegUvRB_IA.jpeg)

The goal of REST is to increase performance, scalability, simplicity, modifiability, visibility, portability, and reliability. This is achieved through following REST principles such as a client–server architecture, statelessness, cacheability, use of a layered system, support for code on demand, and using a uniform interface. These principles must be followed for the system to be classified as REST.

---

# Introduction :

A web service is a collection of open protocols and standards used for exchanging data between applications or systems. Software applications written in various programming languages and running on various platforms can use web services to exchange data over computer networks like the Internet in a manner similar to inter-process communication on a single computer. This interoperability e.g., 
between 
* Java 
* Python 
* Windows and Linux applications 
is due to the use of open standards.

Web services based on REST Architecture are known as RESTful web services. These webservices uses [HTTP methods](https://www.tutorialspoint.com/http/http_methods.htm) to implement the concept of REST architecture. A RESTful web service usually defines a [URI](https://www.tutorialspoint.com/restful/restful_addressing.htm), Uniform Resource Identifier a service, provides resource representation such as JSON and set of HTTP Methods.

![pic](https://www.educative.io/api/collection/6064040858091520/6411938009448448/page/6353247012913152/image/6125166331428864)

**key points** : World wide web,  hypermedia, HTTP methods, URI(Uniform Resource Identifier), SOAP , XML  , JSON.

---

# Work Done :

**There are many things you can done using REST (or REST API)** :


* Unit Testing Rest Services with Spring Boot and JUnit.
    
* Writing Integrateion Tsts for Rest Services with Spring Boot.

* Spring Boot Exception(Error) Handling for RESTful Services.

---
## Example:

using REST API just sign-in into a server :

1. First you need Tableau v 9.0 or Tableau online.

2. You need to install [postman](https://www.postman.com/downloads/).

## copy this code for sign-in into your server :

``` 
<tsRequest>
    <credentials name>="xyz@tableau.com" password="xyz">
        <site contentUrl>=your sitename/>
    </credentials>
</tsRequest>
```
[more reference](https://youtu.be/rlKakxlIB1I)

---
## Some of the advantages of REST web services are:

1. Learning curve is easy since it works on HTTP protocol.
2. Supports multiple technologies for data transfer such as text, xml, json, image etc.
3. No contract defined between server and client, so loosely coupled implementation.
4. REST is a lightweight protocol.


# Discussion :

Unlike [SOAP](https://en.wikipedia.org/wiki/SOAP)-based web services, there is no "official" standard for RESTful web APIs. This is because REST is an architectural style, while SOAP is a protocol. REST is not a standard in itself, but RESTful implementations make use of standards, such as [HTTP](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol), [URI](https://en.wikipedia.org/wiki/Uniform_Resource_Identifier), [JSON](https://en.wikipedia.org/wiki/JSON), and [XML](https://en.wikipedia.org/wiki/XML). Many developers describe their APIs as being RESTful, even though these APIs do not fulfill all of the architectural constraints described above (especially the uniform interface constraint).

---

# References :

1. [Wikipedia of REST](https://en.wikipedia.org/wiki/Representational_state_transfer)


2. [HTTP methods](https://www.tutorialspoint.com/http/http_methods.htm)
3. [some examples of projects using tools](https://www.springboottutorial.com/spring-boot-rest-api-projects-with-code-examples)

4. [Hypermedia](https://en.wikipedia.org/wiki/Hypermedia)

5. [JSON](https://en.wikipedia.org/wiki/JSON)

6. [URI](https://en.wikipedia.org/wiki/Uniform_Resource_Identifier)

7. [RESTful web-services using **JAVA**](https://www.tutorialspoint.com/restful/restful_first_application.htm)

8. [RESTful web services methods](https://www.tutorialspoint.com/restful/restful_methods.htm)

9. [**For more about REST**](https://www.tutorialspoint.com/restful/index.htm)

